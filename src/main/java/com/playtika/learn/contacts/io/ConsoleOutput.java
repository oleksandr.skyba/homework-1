package com.playtika.learn.contacts.io;

import com.playtika.learn.contacts.io.Output;
import lombok.NonNull;

import java.io.PrintStream;

public class ConsoleOutput implements Output {

    private PrintStream stream;

    public ConsoleOutput(@NonNull PrintStream printStream) {
        stream = printStream;
    }

    @Override
    public void printMessage(@NonNull String commandMessage, Object... args) {

        stream.println(String.format(commandMessage, args));
    }
}
