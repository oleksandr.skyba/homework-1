package com.playtika.learn.contacts.io;

import com.playtika.learn.contacts.exceptions.QuitException;
import lombok.Getter;
import org.javatuples.Pair;

import java.io.InputStream;
import java.util.Scanner;

public class ConsoleInput implements Input {

    private Scanner scanner;
    @Getter
    private String quitWord;

    public ConsoleInput(String quitWord, InputStream inputStream) {
        this.quitWord = quitWord;
        scanner = new Scanner(inputStream);
    }

    @Override
    public Pair<Integer, Boolean> getInt() throws QuitException {
        var stringInput = getString();

        if (stringInput.getValue1())
            return new Pair<>(0, true);

        try {
            Integer value = Integer.valueOf(stringInput.getValue0());
            return new Pair<>(value, false);
        }
        catch(NumberFormatException e) {
            return new Pair<>(0, true);
        }
    }

    @Override
    public Pair<Long, Boolean> getLong() throws QuitException {
        var stringInput = getString();

        if (stringInput.getValue1())
            return new Pair<>(0L, true);

        try {
            Long value = Long.valueOf(stringInput.getValue0());
            return new Pair<>(value, false);
        }
        catch(NumberFormatException e) {
            return new Pair<>(0L, true);
        }
    }

    @Override
    public Pair<String, Boolean> getString() throws QuitException {

        if (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (quitWord.equals(line))
                throw new QuitException();

            return new Pair<>(line, false);
        }

        return new Pair<>("", true);
    }
}
