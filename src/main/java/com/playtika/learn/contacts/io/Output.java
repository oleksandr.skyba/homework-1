package com.playtika.learn.contacts.io;

import lombok.NonNull;

public interface Output {

    void printMessage(@NonNull String commandMessage, Object... args);
}
