package com.playtika.learn.contacts;

import com.playtika.learn.contacts.commands.MenuCommand;
import com.playtika.learn.contacts.exceptions.QuitException;
import com.playtika.learn.contacts.io.Input;
import com.playtika.learn.contacts.io.Output;

import java.util.HashMap;
import java.util.Map;

public class ConsoleMenu {

    private Map<Integer, MenuCommand> commands;
    private Input input;
    private Output output;

    ConsoleMenu(Map<Integer, MenuCommand> commands, Output output, Input input) {
        this.commands = commands;
        this.input = input;
        this.output = output;
    }

    public void start(ContactsContainer contactsContainer) {

        String quitWord = input.getQuitWord();

        try {
            while (true) {
                printCommands(commands);
                output.printMessage("Choose a command or '%s' to escape", quitWord);

                var input = this.input.getInt();

                while (input.getValue1() || !commands.containsKey(input.getValue0())) {
                    printCommands(commands);
                    output.printMessage("Enter valid number or '%s' to escape", quitWord);

                    input = this.input.getInt();
                }

                var command = commands.get(input.getValue0());

                contactsContainer = command.act(contactsContainer);
                output.printMessage("");
            }
        }
        catch (QuitException e) {}
    }

    private void printCommands(Map<Integer, MenuCommand> commands) {

        for (Map.Entry<Integer, MenuCommand> entry: commands.entrySet() ) {

            output.printMessage("%d - %s", entry.getKey(), entry.getValue().getCommandDescription());
        }
    }
}
