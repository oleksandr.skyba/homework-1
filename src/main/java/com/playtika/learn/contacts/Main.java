package com.playtika.learn.contacts;

import com.playtika.learn.contacts.commands.*;
import com.playtika.learn.contacts.io.ConsoleInput;
import com.playtika.learn.contacts.io.ConsoleOutput;
import com.playtika.learn.contacts.io.Input;
import com.playtika.learn.contacts.io.Output;

import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {

        Output output = new ConsoleOutput(System.out);
        Input input = new ConsoleInput("quit", System.in);
        ContactsContainer contactsBook = new ContactsBook();
        var commands = createCommandsList(output, input);

        ConsoleMenu menu = new ConsoleMenu(commands, output, input);
        menu.start(contactsBook);
    }

    private static HashMap<Integer, MenuCommand> createCommandsList(Output output, Input input) {

        ArrayList<MenuCommand> commands = new ArrayList<>();
        commands.add(new CommandAdd(input, output));
        commands.add(new CommandShowAll(input, output));
        commands.add(new CommandDetails(input, output));
        commands.add(new CommandNameSearch(input, output));
        commands.add(new CommandDelete(input, output));

        HashMap<Integer, MenuCommand> menu = new HashMap<>();
        for (var command : commands) {
            menu.put(command.getCommandId().getId(), command);
        }

        return menu;
    }
}
