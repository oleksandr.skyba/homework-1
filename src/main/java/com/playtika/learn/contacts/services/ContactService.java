package com.playtika.learn.contacts.services;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.playtika.learn.contacts.Contact;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

@RequiredArgsConstructor
public class ContactService {
    private final ObjectMapper objectMapper;
    private final HttpClient httpClient;
    private final String baseLink;

    @SneakyThrows
    public List<Contact> getContacts() {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(baseLink + "/contacts"))
                .build();

        String body = httpClient.send(request, HttpResponse.BodyHandlers.ofString()).body();

        return objectMapper.readValue(body, new TypeReference<List<Contact>>() { });
    }

    @SneakyThrows
    public Contact getContact(long id) {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(baseLink + "/contacts/" + id))
                .build();

        String body = httpClient.send(request, HttpResponse.BodyHandlers.ofString()).body();

        return objectMapper.readValue(body, new TypeReference<Contact>() { });
    }

    @SneakyThrows
    public void addContacts(List<Contact> contacts) {
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(contacts)))
                .header("Content-Type", "application/json")
                .uri(URI.create(baseLink + "/contacts"))
                .build();

        httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    }
}
