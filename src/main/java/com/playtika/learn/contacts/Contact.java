package com.playtika.learn.contacts;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class Contact {
    private String name;
    private String surname;
    private List<String> emails;
    private List<Long> phones;
    private Long id;

    public Contact() {
        emails = new ArrayList<>();
        phones = new ArrayList<>();
        name = "";
        surname = "";
    }

    @JsonIgnore
    public String getFullName() {
        return name + " " + surname;
    }
}
