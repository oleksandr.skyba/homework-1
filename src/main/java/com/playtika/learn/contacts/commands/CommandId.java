package com.playtika.learn.contacts.commands;

import lombok.Getter;

public enum CommandId {

    ADD (1),
    DETAILS (2),
    SHOW_ALL (3),
    SEARCH_BY_NAME (4),
    DELETE (5);

    @Getter
    private int id;

    private CommandId(int id) {
        this.id = id;
    }

}
