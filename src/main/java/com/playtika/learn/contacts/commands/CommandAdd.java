package com.playtika.learn.contacts.commands;

import com.playtika.learn.contacts.*;
import com.playtika.learn.contacts.exceptions.QuitException;
import com.playtika.learn.contacts.io.Input;
import com.playtika.learn.contacts.io.Output;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandAdd implements MenuCommand {
    private Output output;
    private Input input;
    private String quitWord;

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private Boolean isValidEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public CommandAdd(Input input, Output output) {
        this.input = input;
        this.output = output;
        quitWord = input.getQuitWord();
    }

    @Override
    public ContactsContainer act(ContactsContainer contactsBook) {

        Contact newContact = new Contact();
        try {
            String name = readNamePart("name");
            newContact.setName(name);

            String surname = readNamePart("surname");
            newContact.setSurname(surname);

            var telephones = readTelephones();
            newContact.setPhones(telephones);

            var emails = readEmails();
            newContact.setEmails(emails);

            contactsBook.addContact(newContact);
        }
        catch (QuitException e) {}

        return contactsBook;
    }

    private String readNamePart(String namePartTitle) throws QuitException {

        output.printMessage("Enter %s or '%s' to escape", namePartTitle, quitWord);
        var input = this.input.getString();
        while (input.getValue1()) {
            output.printMessage("Provide valid %s or '%s' to escape", namePartTitle, quitWord);
            input = this.input.getString();
        }

        return input.getValue0();
    }

    private ArrayList<Long> readTelephones() throws QuitException {

        ArrayList<Long> numbers = new ArrayList<>();
        Long number = readTelephone("Enter telephone or '%s' to escape",
                "Provide valid telephone or '%s' to escape");
        numbers.add(number);

        try {
            while (true) {
                number = readTelephone("Enter telephone or '%s' to continue",
                        "Provide valid telephone or '%s' to continue");
                numbers.add(number);
            }
        }
        catch (QuitException e) {}

        return numbers;
    }

    private Long readTelephone(String messageTemplate, String failureMessageTemplate) throws QuitException {

        output.printMessage(messageTemplate, quitWord);
        var input = this.input.getLong();
        while (input.getValue1() || input.getValue0() < 1) {
            output.printMessage(failureMessageTemplate, quitWord);
            input = this.input.getLong();
        }
        return input.getValue0();
    }

    private ArrayList<String> readEmails() throws QuitException {

        ArrayList<String> numbers = new ArrayList<>();
        String email = readEmail("Enter email or '%s' to escape",
                "Provide valid email or '%s' to escape");
        numbers.add(email);

        try {
            email = readEmail("Enter email or '%s' to continue",
                    "Provide valid email or '%s' to continue");
            numbers.add(email);
        }
        catch (QuitException e) {}

        return numbers;
    }

    private String readEmail(String messageTemplate, String failureMessageTemplate) throws QuitException {

        output.printMessage(messageTemplate, quitWord);
        var input = this.input.getString();
        while (input.getValue1() || !isValidEmail(input.getValue0())) {
            output.printMessage(failureMessageTemplate, quitWord);
            input = this.input.getString();
        }
        return input.getValue0();
    }

    @Override
    public CommandId getCommandId() {
        return CommandId.ADD;
    }

    @Override
    public String getCommandDescription() {
        return "Add new contact";
    }
}
