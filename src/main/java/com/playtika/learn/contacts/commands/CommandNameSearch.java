package com.playtika.learn.contacts.commands;

import com.playtika.learn.contacts.*;
import com.playtika.learn.contacts.exceptions.QuitException;
import com.playtika.learn.contacts.io.Input;
import com.playtika.learn.contacts.io.Output;

import java.util.Locale;
import java.util.function.Predicate;

public class CommandNameSearch implements MenuCommand {
    private Output output;
    private Input input;
    private String quitWord;

    public CommandNameSearch(Input input, Output output) {
        this.input = input;
        this.output = output;
        quitWord = input.getQuitWord();
    }

    @Override
    public ContactsContainer act(ContactsContainer contactsBook) {
        output.printMessage("Enter chars to search for or '%s' to escape", quitWord);

        try {
            var input = this.input.getString();
            while (input.getValue1()) {
                output.printMessage("Enter valid chars or '%s' to escape", quitWord);
                input = this.input.getString();
            }
            String searchedChars = input.getValue0();
            Predicate<Contact> predicate = (contact) -> contact.getFullName().toLowerCase(Locale.ROOT).contains(searchedChars);

            for (Contact contact: contactsBook.getContacts(predicate)) {
                output.printMessage("%d %s", contact.getId(), contact.getFullName());
            }
        }
        catch (QuitException e) {}


        return contactsBook;
    }

    @Override
    public CommandId getCommandId() {
        return CommandId.SEARCH_BY_NAME;
    }

    @Override
    public String getCommandDescription() {
        return "Search contacts by name";
    }
}
