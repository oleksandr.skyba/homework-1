package com.playtika.learn.contacts.commands;

import com.playtika.learn.contacts.*;
import com.playtika.learn.contacts.exceptions.QuitException;
import com.playtika.learn.contacts.io.Input;
import com.playtika.learn.contacts.io.Output;

public class CommandDelete implements MenuCommand {
    private Output output;
    private Input input;
    private String quitWord;

    public CommandDelete(Input input, Output output) {
        this.input = input;
        this.output = output;
        quitWord = input.getQuitWord();
    }

    @Override
    public ContactsContainer act(ContactsContainer contactsBook) {
        output.printMessage("Enter id of contact or '%s' to escape", quitWord);

        try {
            var input = this.input.getLong();
            Boolean isWrongValue = input.getValue1();

            while (isWrongValue || contactsBook.deleteContact(input.getValue0()) == false) {
                output.printMessage("Wrong id, please enter existing id or '%s' to escape", quitWord);

                input = this.input.getLong();
                isWrongValue = input.getValue1();
            }
        }
        catch (QuitException e) {}

        return contactsBook;
    }

    @Override
    public CommandId getCommandId() {
        return CommandId.DELETE;
    }

    @Override
    public String getCommandDescription() {
        return "Delete contact";
    }
}
