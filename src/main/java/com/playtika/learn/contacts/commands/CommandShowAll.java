package com.playtika.learn.contacts.commands;

import com.playtika.learn.contacts.*;
import com.playtika.learn.contacts.io.Input;
import com.playtika.learn.contacts.io.Output;

public class CommandShowAll implements MenuCommand {
    private Output output;
    private Input input;

    public CommandShowAll(Input input, Output output) {
        this.input = input;
        this.output = output;
    }

    @Override
    public ContactsContainer act(ContactsContainer contactsBook) {

        if (contactsBook.isEmpty()) {
            output.printMessage("No contacts to show");
            return contactsBook;
        }

        for (Contact contact: contactsBook.getAllContacts()) {
            output.printMessage("%d %s", contact.getId(), contact.getFullName());
        }

        return contactsBook;
    }

    @Override
    public CommandId getCommandId() {
        return CommandId.SHOW_ALL;
    }

    @Override
    public String getCommandDescription() {
        return "Show all contacts";
    }
}
