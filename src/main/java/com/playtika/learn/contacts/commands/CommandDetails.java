package com.playtika.learn.contacts.commands;

import com.playtika.learn.contacts.*;
import com.playtika.learn.contacts.exceptions.QuitException;
import com.playtika.learn.contacts.io.Input;
import com.playtika.learn.contacts.io.Output;

import java.util.stream.Collectors;

public class CommandDetails implements MenuCommand {
    private Output output;
    private Input input;

    private String quitWord;

    public CommandDetails(Input input, Output output) {
        this.input = input;
        this.output = output;
        quitWord = input.getQuitWord();
    }

    @Override
    public ContactsContainer act(ContactsContainer contactsBook) {
        output.printMessage("Enter contact id or '%s' to escape", quitWord);
        Contact foundContact = null;
        try {
            while (foundContact == null) {

                var input = this.input.getLong();

                while (input.getValue1()) {
                    output.printMessage("Enter correct contact id or '%s' to escape", quitWord);
                    input = this.input.getLong();
                }

                foundContact = contactsBook.getContact(input.getValue0());
                if (foundContact == null) {
                    output.printMessage("Enter existing contact id or '%s' to escape", quitWord);
                }
            }
        }
        catch (QuitException e) {}

        printContact(foundContact);

        return contactsBook;
    }

    private void printContact(Contact contact) {
        if (contact == null)
            return;

        output.printMessage("Name: %s", contact.getName());
        output.printMessage("Surname: %s", contact.getSurname());
        output.printMessage("Id: %s", contact.getId());
        output.printMessage("Emails: %s", String.join(", ", contact.getEmails()));
        var phones = contact.getPhones().stream()
                .map(Object::toString)
                .collect(Collectors.toList());
        output.printMessage("Phones: %s", String.join(", ", phones));
    }

    @Override
    public CommandId getCommandId() {
        return CommandId.DETAILS;
    }

    @Override
    public String getCommandDescription() {
        return "Show contact details";
    }
}
