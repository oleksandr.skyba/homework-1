package com.playtika.learn.contacts.commands;

import com.playtika.learn.contacts.Contact;
import com.playtika.learn.contacts.ContactsContainer;
import com.playtika.learn.contacts.exceptions.QuitException;
import com.playtika.learn.contacts.io.Input;
import com.playtika.learn.contacts.io.Output;
import lombok.SneakyThrows;
import org.javatuples.Pair;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CommandAddTest {

    @Mock
    Input input;

    @Mock
    Output output;

    @Mock
    ContactsContainer contactsContainer;

    CommandAdd command;

    @BeforeEach
    void init() {
        when(input.getQuitWord()).thenReturn("quit");

        command = new CommandAdd(input, output);
    }

    @Test
    void shouldGetCommandId() {
        assertThat(command.getCommandId()).isEqualTo(CommandId.ADD);
    }

    @Test
    void shouldGetCommandDescription() {
        assertThat(command.getCommandDescription()).isEqualTo("Add new contact");
    }

    @SneakyThrows
    @Test
    void shouldAddContact() {
        final List<Contact> contacts = new ArrayList<>();
        doAnswer((invocationOnMock) -> {
            Contact contact = (Contact) invocationOnMock.getArgument(0);
            contacts.add(contact);
            return null;
        }).when(contactsContainer).addContact(any(Contact.class));

        when(input.getString())
                .thenReturn(new Pair<>("name", false))
                .thenReturn(new Pair<>("surname", false))
                .thenReturn(new Pair<>("mail1@mail.com", false))
                .thenReturn(new Pair<>("mail2@mail.com", false))
                .thenThrow(new QuitException());
        when(input.getLong())
                .thenReturn(new Pair<>(11111111L, false))
                .thenReturn(new Pair<>(22222222L, false))
                .thenThrow(new QuitException());

        var container = command.act(contactsContainer);

        assertThat(contacts.size()).isEqualTo(1);
    }

    @SneakyThrows
    @Test
    void shouldAddContactWithSeveralTries() {
        final List<Contact> contacts = new ArrayList<>();
        doAnswer((invocationOnMock) -> {
            Contact contact = (Contact) invocationOnMock.getArgument(0);
            contacts.add(contact);
            return null;
        }).when(contactsContainer).addContact(any(Contact.class));

        when(input.getString())
                .thenReturn(new Pair<>("badname", true))
                .thenReturn(new Pair<>("name", false))
                .thenReturn(new Pair<>("surname", false))
                .thenReturn(new Pair<>("mail1@mail.com", false))
                .thenReturn(new Pair<>("123", true))
                .thenThrow(new QuitException());
        when(input.getLong())
                .thenReturn(new Pair<>(-100000000L, true))
                .thenReturn(new Pair<>(22222222L, false))
                .thenThrow(new QuitException());

        var container = command.act(contactsContainer);

        assertThat(contacts.size()).isEqualTo(1);
        verify(output, atLeastOnce()).printMessage(startsWith("Provide valid"), anyString(), anyString());
    }
}