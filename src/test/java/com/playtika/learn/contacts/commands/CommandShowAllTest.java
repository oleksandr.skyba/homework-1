package com.playtika.learn.contacts.commands;

import com.playtika.learn.contacts.ContactsContainer;
import com.playtika.learn.contacts.io.Input;
import com.playtika.learn.contacts.io.Output;
import org.javatuples.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.playtika.learn.contacts.TestHelper.createContacts;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CommandShowAllTest {

    @Mock
    Input input;

    @Mock
    Output output;

    @Mock
    ContactsContainer contactsContainer;

    @InjectMocks
    CommandShowAll command;

    @Test
    void shouldShowContacts() {
        when(contactsContainer.getAllContacts()).thenReturn(createContacts(List.of(
                new Pair<>("anna", "kaplun"),
                new Pair<>("serhii", "kaplan"),
                new Pair<>("oleh", "taran"),
                new Pair<>("maksym", "overko"))));

        var container = command.act(contactsContainer);

        verify(output, times(4)).printMessage(startsWith("%d %s"), anyLong(), anyString());
    }

    @Test
    void shouldNotShowContactsEmptyList() {
        when(contactsContainer.isEmpty()).thenReturn(true);

        var container = command.act(contactsContainer);

        verify(output, never()).printMessage(startsWith("%d %s"), anyLong(), anyString());
        verify(output, times(1)).printMessage("No contacts to show");
    }

    @Test
    void shouldGetCommandId() {
        assertThat(command.getCommandId()).isEqualTo(CommandId.SHOW_ALL);
    }

    @Test
    void shouldGetCommandDescription() {
        assertThat(command.getCommandDescription()).isEqualTo("Show all contacts");
    }
}