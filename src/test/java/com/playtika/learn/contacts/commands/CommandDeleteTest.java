package com.playtika.learn.contacts.commands;

import com.playtika.learn.contacts.Contact;
import com.playtika.learn.contacts.ContactsContainer;
import com.playtika.learn.contacts.exceptions.QuitException;
import com.playtika.learn.contacts.io.Input;
import com.playtika.learn.contacts.io.Output;
import lombok.SneakyThrows;
import org.javatuples.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CommandDeleteTest {
    @Mock
    Input input;

    @Mock
    Output output;

    @Mock
    ContactsContainer contactsContainer;

    CommandDelete command;

    @BeforeEach
    void init() {
        when(input.getQuitWord()).thenReturn("quit");

        command = new CommandDelete(input, output);
    }

    @Test
    void shouldGetCommandId() {
        assertThat(command.getCommandId()).isEqualTo(CommandId.DELETE);
    }

    @Test
    void shouldGetCommandDescription() {
        assertThat(command.getCommandDescription()).isEqualTo("Delete contact");
    }

    @Test
    @SneakyThrows
    void shouldDeleteContact() {
        when(input.getLong()).thenReturn(new Pair<>(111L, false));

        when(contactsContainer.deleteContact(anyLong())).thenReturn(true);

        var updatedContacts = command.act(contactsContainer);

        verify(contactsContainer).deleteContact(111L);
    }

    @Test
    @SneakyThrows
    void sholdDeleteContactFromSecondTry() {
        when(input.getLong())
                .thenReturn(new Pair<>(123L, false))
                .thenReturn(new Pair<>(111L, false));

        when(contactsContainer.deleteContact(123L)).thenReturn(false);
        when(contactsContainer.deleteContact(111L)).thenReturn(true);

        var updatedContacts = command.act(contactsContainer);

        verify(contactsContainer).deleteContact(111L);
    }

    @Test
    @SneakyThrows
    void sholdNotDeleteContact() {
        when(input.getLong()).thenReturn(new Pair<>(111L, true)).thenThrow(new QuitException());

        var updatedContacts = command.act(contactsContainer);

        verify(contactsContainer, never()).deleteContact(111L);
        verify(output, times(1))
                .printMessage(startsWith("Wrong id, please enter existing id"),anyString());
    }
}