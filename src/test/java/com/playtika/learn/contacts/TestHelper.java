package com.playtika.learn.contacts;

import org.javatuples.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TestHelper {
    public static List<Contact> createContacts(List<Pair<String, String>> names) {
        List<Contact> contacts = new ArrayList<>();

        long counter = 1L;
        for (var namePair: names) {
            Contact contact = new Contact();
            contact.setName(namePair.getValue0());
            contact.setSurname(namePair.getValue1());
            contact.setId(counter);
            contact.setEmails(List.of(String.format("mail%d@mail.com", counter)));
            contact.setPhones(List.of(counter * 200L));

            contacts.add(contact);

            counter++;
        }

        return contacts;
    }

    public static List<Contact> createContacts(int countOfContacts) {

        var names = new ArrayList<Pair<String, String>>();
        for (int i=0; i < countOfContacts; i++) {
            names.add(new Pair<>("name_" + i, "surname_i"));
        }

        return createContacts(names);
    }
}
