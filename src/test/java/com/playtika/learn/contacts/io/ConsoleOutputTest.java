package com.playtika.learn.contacts.io;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ConsoleOutputTest {

    @Mock
    PrintStream printStream;

    @InjectMocks
    ConsoleOutput consoleOutput;

    @ParameterizedTest
    @ValueSource(strings = {"one", "two", "three"})
    void shouldPrintMessage(String value) {

        consoleOutput.printMessage(value);

        verify(printStream).println(value);
    }

    @ParameterizedTest
    @ValueSource(strings = {"one %d", "two %d", "%d"})
    void shouldPrintMessageWithArgs(String string) {

        consoleOutput.printMessage(string, 2);

        verify(printStream).println(String.format(string, 2));
    }
}