package com.playtika.learn.contacts.io;

import com.playtika.learn.contacts.exceptions.QuitException;
import lombok.SneakyThrows;
import org.javatuples.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.spy;

class ConsoleInputTest {

    @Test
    @SneakyThrows
    void shouldGetLong() {

        var inputStream = new ByteArrayInputStream("123\n".getBytes());
        var input = new ConsoleInput("exit", inputStream);

        var result = input.getLong();

        assertThat(result.getValue0()).isEqualTo(123L);
        assertThat(result.getValue1()).isFalse();
    }

    @Test
    void shouldGetLongThrowsQuitException() {

        var inputStream = new ByteArrayInputStream("exit\n".getBytes());
        var input = new ConsoleInput("exit", inputStream);

        assertThatThrownBy(()->input.getLong()).isInstanceOf(QuitException.class);
    }

    @Test
    @SneakyThrows
    void shouldNotGetLong() {

        var inputStream = new ByteArrayInputStream("boom\n".getBytes());
        var input = new ConsoleInput("exit", inputStream);

        Pair<Long, Boolean> result = input.getLong();

        assertThat(result.getValue1()).isTrue();
    }

    @Test
    @SneakyThrows
    void shouldGetInt() {

        var inputStream = new ByteArrayInputStream("123\n".getBytes());
        var input = new ConsoleInput("exit", inputStream);

        var result = input.getInt();

        assertThat(result.getValue0()).isEqualTo(123);
        assertThat(result.getValue1()).isFalse();
    }

    @Test
    void shouldGetIntThrowsQuitException() {

        var inputStream = new ByteArrayInputStream("exit\n".getBytes());
        var input = new ConsoleInput("exit", inputStream);

        assertThatThrownBy(() -> input.getInt()).isInstanceOf(QuitException.class);
    }

    @Test
    @SneakyThrows
    void shouldNotGetInt() {

        var inputStream = new ByteArrayInputStream("boom\n".getBytes());
        var input = new ConsoleInput("exit", inputStream);

        var result = input.getInt();

        assertThat(result.getValue1()).isTrue();
    }

    @Test
    @SneakyThrows
    void shouldGetString() {

        var inputStream = new ByteArrayInputStream("string\n".getBytes());
        var input = new ConsoleInput("exit", inputStream);

        var result = input.getString();

        assertThat(result.getValue0()).isEqualTo("string");
        assertThat(result.getValue1()).isFalse();
    }

    @Test
    void shouldGetStringThrowsQuitException() {

        var inputStream = new ByteArrayInputStream("exit\n".getBytes());
        var input = new ConsoleInput("exit", inputStream);

        assertThatThrownBy(()->input.getString()).isInstanceOf(QuitException.class);
    }

    @ParameterizedTest
    @ValueSource(strings = {"exit","quit","stop"})
    void shouldGetQuitWord(String value) {
        var inputStream = new ByteArrayInputStream("exit\n".getBytes());
        var input = new ConsoleInput(value, inputStream);

        assertThat(input.getQuitWord()).isEqualTo(value);
    }
}