package com.playtika.learn.contacts;

import com.playtika.learn.contacts.commands.CommandId;
import com.playtika.learn.contacts.commands.MenuCommand;
import com.playtika.learn.contacts.exceptions.QuitException;
import com.playtika.learn.contacts.io.Input;
import com.playtika.learn.contacts.io.Output;
import lombok.SneakyThrows;
import org.javatuples.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ConsoleMenuTest {
    @Mock
    Input input;

    @Mock
    Output output;

    @Mock
    ContactsContainer contactsContainer;

    @Mock
    MenuCommand addCommand;
    @Mock
    MenuCommand deleteCommand;

    Map<Integer, MenuCommand> commands;

    @BeforeEach
    void init() {
        commands = new HashMap<>();
        commands.put(CommandId.ADD.getId(), addCommand);
        commands.put(CommandId.DELETE.getId(), deleteCommand);

        when(addCommand.getCommandDescription()).thenReturn("Add contact");
        when(deleteCommand.getCommandDescription()).thenReturn("Delete contact");

        when(input.getQuitWord()).thenReturn("quit");
    }


    @Test
    @SneakyThrows
    void shouldPrintAllCommandsDescriptionAndQuit() {
        ConsoleMenu menu = new ConsoleMenu(commands, output, input);
        when(addCommand.getCommandId()).thenReturn(CommandId.ADD);
        when(deleteCommand.getCommandId()).thenReturn(CommandId.DELETE);

        when(input.getInt()).thenThrow(new QuitException());

        menu.start(contactsContainer);

        verify(output).printMessage("%d - %s", addCommand.getCommandId().getId(), addCommand.getCommandDescription());
        verify(output).printMessage("%d - %s", deleteCommand.getCommandId().getId(), deleteCommand.getCommandDescription());
    }

    @Test
    @SneakyThrows
    void shouldCallAddCommandAndQuit() {
        ConsoleMenu menu = new ConsoleMenu(commands, output, input);

        when(input.getInt())
                .thenReturn(new Pair<>(CommandId.ADD.getId(), false))
                .thenThrow(new QuitException());

        menu.start(contactsContainer);

        verify(addCommand).act(contactsContainer);
    }

    @Test
    @SneakyThrows
    void shouldCallDeleteCommandAndQuit() {
        ConsoleMenu menu = new ConsoleMenu(commands, output, input);

        when(input.getInt())
                .thenReturn(new Pair<>(CommandId.DELETE.getId(), false))
                .thenThrow(new QuitException());

        menu.start(contactsContainer);

        verify(deleteCommand).act(contactsContainer);
    }

    @Test
    @SneakyThrows
    void shouldNotFindCommandAndQuit() {
        ConsoleMenu menu = new ConsoleMenu(commands, output, input);

        when(input.getInt())
                .thenReturn(new Pair<>(3, false))
                .thenThrow(new QuitException());

        menu.start(contactsContainer);

        verify(output).printMessage(startsWith("Enter valid number"), anyString());
    }
}